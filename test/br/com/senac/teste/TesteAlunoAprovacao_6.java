 
package br.com.senac.teste;

import Exercicio6.Aluno;
import Exercicio6.VerificarAprovacao;
import org.junit.Test;
import static org.junit.Assert.*;

 
public class TesteAlunoAprovacao_6 {
    
    public TesteAlunoAprovacao_6() {
    }
    
    
    @Test
    public void alunoDeveSerAprovado(){
        
        Aluno aluno = new Aluno(1, "Joao");
        aluno.setNota(7.5);
        
        
        VerificarAprovacao va = new VerificarAprovacao();
        
        assertTrue(va.verificarAlunoAprovado(aluno));
   
    }
    
    @Test
    public void alunoReprovadoNaoDeveSerAprovado(){
        
        Aluno aluno = new Aluno (2,"José");
        aluno.setNota(5);
        
        VerificarAprovacao va = new VerificarAprovacao();
        
        assertFalse(va.verificarAlunoAprovado(aluno));
    }
    
    
    @Test
    public void alunoDeveSerReprovado(){
        
        Aluno aluno = new Aluno (3,"Beto");
        aluno.setNota(3);
        
        VerificarAprovacao va = new VerificarAprovacao();
        
        assertTrue(va.verificarAlunoReprovado(aluno));
    }
    
    @Test 
    public void alunoAporvadoNaoDeveSerAprovado(){
        
        Aluno aluno = new Aluno (4,"Pedro");
        aluno.setNota(8);
        
        VerificarAprovacao va = new VerificarAprovacao();
        
        assertFalse(va.verificarAlunoReprovado(aluno));
        
    }
    
    @Test
    public void alunoDeveEstarDeRecuperacao(){
        
        Aluno aluno = new Aluno();
        aluno.setNota(5);
        
        VerificarAprovacao va = new VerificarAprovacao();
        
        assertTrue(va.verificarAlunoRecuperacao(aluno));
    }
    
    
    @Test
    public void alunoDeveReceberAprovado (){
        
        Aluno aluno = new Aluno(5, "Maria");
        aluno.setNota(8);
        
        VerificarAprovacao va = new VerificarAprovacao();
        
        assertEquals(va.ALUNO_APROVADO, va.resultadoAprovacao(aluno));
        
        
    }
    
    @Test
    public void alunoDeveReceberReprovado (){
        
        Aluno aluno = new Aluno(6, "Mariana");
        aluno.setNota(3.5);
        
        VerificarAprovacao va = new VerificarAprovacao();
        
        assertEquals(va.ALUNO_REPROVADO, va.resultadoAprovacao(aluno));
        
        
    }
    
        @Test
    public void alunoDeveReceberRecuperacao (){
        
        Aluno aluno = new Aluno(6, "Mariangela");
        aluno.setNota(4);
        
        VerificarAprovacao va = new VerificarAprovacao();
        
        assertEquals(va.ALUNO_RECUPERACAO, va.resultadoAprovacao(aluno));
        
        
    }
}
