
package br.com.senac.teste;

import Exercicio1.IdadePessoas;
import Exercicio1.Pessoa;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;


public class TesteIdade_1 {
    
    public TesteIdade_1() {
    }
    
    @Test
    public void deveAnaSerMAisVelha(){
        
        List<Pessoa> lista = new ArrayList<>();
        Pessoa pessoa1 = new Pessoa("Ana", 35);
        Pessoa pessoa2 = new Pessoa("Joana", 12);
        Pessoa pessoa3 = new Pessoa("Maria", 20);

        lista.add(pessoa1);
        lista.add(pessoa2);
        lista.add(pessoa3);
        
        IdadePessoas ip = new IdadePessoas();

        
        assertEquals("Ana", ip.verificarMaisVelho(lista).getNome());
   
    }
    
        @Test
    public void deveAnaNaoSerMAisVelha(){
        
        List<Pessoa> lista = new ArrayList<>();
        Pessoa pessoa1 = new Pessoa("Ana", 10);
        Pessoa pessoa2 = new Pessoa("Joana", 12);
        Pessoa pessoa3 = new Pessoa("Maria", 20);

        lista.add(pessoa1);
        lista.add(pessoa2);
        lista.add(pessoa3);
        
        IdadePessoas ip = new IdadePessoas();

        
        assertNotEquals("Ana", ip.verificarMaisVelho(lista).getNome());
   
    }
    
    
    
        @Test
    public void deveMariaSerMAisNova(){
        
        List<Pessoa> lista = new ArrayList<>();
        Pessoa pessoa1 = new Pessoa("Ana", 10);
        Pessoa pessoa2 = new Pessoa("Joana", 12);
        Pessoa pessoa3 = new Pessoa("Maria", 9);

        lista.add(pessoa1);
        lista.add(pessoa2);
        lista.add(pessoa3);
        
        IdadePessoas ip = new IdadePessoas();

        
        assertEquals("Maria", ip.verificarMaisNovo(lista).getNome());
   
    }
    
         @Test
    public void deveMariaNaoSerMAisNova(){
        
        List<Pessoa> lista = new ArrayList<>();
        Pessoa pessoa1 = new Pessoa("Ana", 10);
        Pessoa pessoa2 = new Pessoa("Joana", 5);
        Pessoa pessoa3 = new Pessoa("Maria", 9);

        lista.add(pessoa1);
        lista.add(pessoa2);
        lista.add(pessoa3);
        
        IdadePessoas ip = new IdadePessoas();

        
        assertNotEquals("Maria", ip.verificarMaisNovo(lista).getNome());
   
    }
    
}
