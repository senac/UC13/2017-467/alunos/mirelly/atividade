package br.com.senac.teste;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Exercicio3.ConversorMoedas;
import Exercicio3.DolarAmericano;
import Exercicio3.DolarAustraliano;
import Exercicio3.Euro;
import Exercicio3.Real;
import org.junit.Test;
import static org.junit.Assert.*;

public class TesteConversao_3 {
    
    public TesteConversao_3() {
    }
    
    
    @Test
    public void deveConverterRealParaDolarAmericano(){
        
        ConversorMoedas cm = new ConversorMoedas();
        Real real = new Real(100);
        DolarAmericano dolarAmericano = new DolarAmericano();
        
        assertEquals(408, cm.converterParaDolarAmericano(real, dolarAmericano).getValor(),0 );
    
    }
    
    
    @Test
    public void deveConverterRealParaEuro(){
        
        ConversorMoedas cm = new ConversorMoedas();
        Real real = new Real (100);
        Euro euro = new Euro();
        
        assertEquals(480, cm.converterParaEuro(real, euro ).getValor(),0);
        
    }
    
    @Test
    public void deverConverterRealPAraDolarAustraliano(){
        
        ConversorMoedas cm = new ConversorMoedas();
        Real real = new Real(100);
        DolarAustraliano dolarAustraliano = new DolarAustraliano();
        
        assertEquals(296, cm.converterParaDolarAustraliano(real,dolarAustraliano).getValor(),0);
        
        
    }
    
}
