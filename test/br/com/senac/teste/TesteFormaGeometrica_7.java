
package br.com.senac.teste;

import Exercicio7.Circulo;
import Exercicio7.Quadrado;
import Exercicio7.Retangulo;
import Exercicio7.Triangulo;
import org.junit.Test;
import static org.junit.Assert.*;

public class TesteFormaGeometrica_7 {
    
    public TesteFormaGeometrica_7() {
        
    }
    
    
    @Test
    public void areaQuadradoDeveSer100 (){
        
        Quadrado quadrado = new Quadrado(10);
        
        assertEquals(100, quadrado.getArea(),0);

    }
    
    
    @Test
    public void areaRetanguloDeveSer150 (){
        
        Retangulo retangulo = new Retangulo(15,10);
        
        assertEquals(150, retangulo.getArea(),0);
        
    }
    
    @Test
    public void areaCirculoDeveSer50e24 (){
        
        Circulo circulo = new Circulo(4);
        
        assertEquals(50.24, circulo.getArea(),0.5);
        
    }
    
    @Test
    public void areaTrianguloDeveSer90 (){
        
        Triangulo triangulo = new Triangulo(3, 60);
        
        assertEquals(90, triangulo.getArea(),0);
    }
}
