
package br.com.senac.teste;


import Exercicio4.FolhaPagamento;
import Exercicio4.ItemVenda;
import Exercicio4.Peca;
import Exercicio4.Venda;
import Exercicio4.Vendedor;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;


public class TesteComissaoVendedor_4 {
    
    public TesteComissaoVendedor_4() {
    }
    
    
    @Test
    public void comissaoVendedorDeveSer5Com2ProdutosDiferentes (){

       Vendedor vendedor = new Vendedor(1,"Joao");

       Peca camisa = new Peca();
       ItemVenda itemVenda = new ItemVenda(camisa, 2, 25);
       Peca meia = new Peca();
       ItemVenda itemVenda1 = new ItemVenda(meia, 5,10);
       
       List<ItemVenda> itens = new ArrayList<>();
       itens.add(itemVenda);
       itens.add(itemVenda1);
       
       Venda venda = new Venda(vendedor);
       
        assertEquals(5, venda.calcularComissao(itens), 0);
    
    }
    
    @Test
    public void comissaoVendedorDeveSer50Com4ProdutosIguais (){

       Vendedor vendedor = new Vendedor(2,"Jose");

       Peca camisa = new Peca();
       ItemVenda itemVenda = new ItemVenda(camisa, 1, 250);
       ItemVenda itemVenda2 = new ItemVenda(camisa, 1, 250);
       ItemVenda itemVenda3 = new ItemVenda(camisa, 1, 250);
       ItemVenda itemVenda4 = new ItemVenda(camisa, 1, 250);
       
       List<ItemVenda> itens = new ArrayList<>();
       itens.add(itemVenda);
       itens.add(itemVenda2);
       itens.add(itemVenda3);
       itens.add(itemVenda4);
       
       Venda venda = new Venda(vendedor);
       
        assertEquals(50, venda.calcularComissao(itens), 0);
    
    }
    
    @Test
    public void deveCalcularComissaoParaJoseEMiguel(){
        
        Vendedor jose = new Vendedor(1, "Jose");
        Vendedor miguel = new Vendedor(2, "Miguel");
        
        Peca peca = new Peca (1, "Blusa");
        
        
        Venda venda1 = new Venda(jose) ;
        venda1.adicionarItem(peca , 10 , 10 ) ;
        
        Venda venda2 = new Venda(miguel);
        venda2.adicionarItem(peca, 10, 20);
        
        List<Venda> listaVendas = new ArrayList<>() ; 
        listaVendas.add(venda1);
        listaVendas.add(venda2);
        
        FolhaPagamento folhaPagamento = new FolhaPagamento() ; 
        folhaPagamento.calcularComissao(listaVendas) ; 
        double comissaoJose = folhaPagamento.getComissaoVendedor(jose) ; 
        
        assertEquals(5.0, comissaoJose , 0.01);
        

}
    
}
