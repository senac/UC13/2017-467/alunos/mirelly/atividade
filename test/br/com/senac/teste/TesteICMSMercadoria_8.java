
package br.com.senac.teste;

import Exercicio8.CalculadoraICMS;
import Exercicio8.Cliente;
import Exercicio8.Mercadoria;
import Exercicio8.Venda;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;


public class TesteICMSMercadoria_8 {
    
    public TesteICMSMercadoria_8() {
    }

@Test
public void devCalcularICMS (){
    
    Cliente cliente = new Cliente(1,"Joao");
    Mercadoria livroA = new Mercadoria(50, 2);
    Mercadoria livroB = new Mercadoria(100, 1);
    
    Venda venda = new Venda(cliente, "RJ");
    List<Mercadoria> lista = new ArrayList<>();
    lista.add(livroA);
    lista.add(livroB);

    venda.fecharVenda(lista);
    
    CalculadoraICMS c = new CalculadoraICMS();
    
    assertEquals(34, c.calcularICMS(venda), 0);

}


    
}
