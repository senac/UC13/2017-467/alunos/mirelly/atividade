
package br.com.senac.teste;

import Exercicio9.Viagem;
import org.junit.Test;
import static org.junit.Assert.*;


public class TesteViagemConsumo_9 {
    
    public TesteViagemConsumo_9() {
    }

@Test
public void devCalcularConsumo (){
    
    Viagem viagem = new Viagem(5, 100);
    
    assertEquals(41.66, viagem.calcularLitros() , 0.9);
 
}

    
}
