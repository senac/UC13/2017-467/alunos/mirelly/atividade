
package br.com.senac.teste;

import Exercicio10.Alunos;
import Exercicio10.CalculoAlunos;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class TesteMediaAlunos_10 {
    
    public TesteMediaAlunos_10() {
    }
    

    
    @Test
    public void deveTer1AcimaDaMedia (){

        Alunos a = new Alunos("a", 5, 6);
        Alunos b = new Alunos("b", 8, 10);
        Alunos c = new Alunos("c", 5, 5);
        
        List<Alunos> lista = new ArrayList<>();
        lista.add(a);
        lista.add(b);
        lista.add(c);

        CalculoAlunos ca = new CalculoAlunos();
                
        assertEquals(1,ca.verificarMedia(lista),0 );
    }
    
    @Test
    public void deveTerNenhumAcimaDaMedia(){
        
         Alunos a = new Alunos("a", 5, 6);
        Alunos b = new Alunos("b", 2, 10);
        Alunos c = new Alunos("c", 5, 5);
        
        List<Alunos> lista = new ArrayList<>();
        lista.add(a);
        lista.add(b);
        lista.add(c);

        CalculoAlunos ca = new CalculoAlunos();
                
        assertEquals(0,ca.verificarMedia(lista),0 );
    
    }
    
     @Test
    public void deveTerTodosAcimaDaMedia(){
        
         Alunos a = new Alunos("a", 8, 6);
        Alunos b = new Alunos("b", 6, 10);
        Alunos c = new Alunos("c", 10, 8);
        
        List<Alunos> lista = new ArrayList<>();
        lista.add(a);
        lista.add(b);
        lista.add(c);

        CalculoAlunos ca = new CalculoAlunos();
                
        assertEquals(lista.size(),ca.verificarMedia(lista),0 );
    
    }
    
}
