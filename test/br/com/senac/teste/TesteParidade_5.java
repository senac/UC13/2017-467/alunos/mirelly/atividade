
package br.com.senac.teste;

import Exercicio5.Paridade;
import org.junit.Test;
import static org.junit.Assert.*;


public class TesteParidade_5 {
    
    public TesteParidade_5() {
    }
    
    
    @Test
    public void deveSerNumeroPar(){
        
        Paridade paridade = new Paridade();
        int numero = 2 ;
        
        assertTrue(paridade.ehNumeroPar(numero));

    }
    
    @Test
    public void deveSerNumeroImpar(){
        Paridade paridade = new Paridade();
        int numero = 3 ;
        
        assertTrue(paridade.ehNumeroImpar(numero));
     
    }
    
    @Test
    public void numeroParNaoDeveSerImpar(){
        Paridade paridade = new Paridade();
        int numero = 2 ;
        
        assertFalse(paridade.ehNumeroImpar(numero));
        
        
    }
    
     @Test
    public void numeroImparNaoDeveSerPar(){
        
        Paridade paridade = new Paridade();
        int numero = 3 ;
        
        assertFalse(paridade.ehNumeroPar(numero));

    }
    
    @Test
    public void deveSerNumeroNegativo(){
        
        Paridade paridade = new Paridade();
        int numero = -5 ;
        
        assertTrue(paridade.ehNumeroNegativo(numero));
    }
    
    @Test
    public void numeroPositivoNaoDeveSerNegativo(){
        
        Paridade paridade = new Paridade();
        int numero = 5 ;
        
        assertFalse(paridade.ehNumeroNegativo(numero));
    }
    
    @Test
    public void deveSerNumeroPositivo(){
        
        Paridade paridade = new Paridade();
        int numero = 6 ;
        
        assertTrue(paridade.ehNumeroPositivo(numero));
    }
    
     @Test
    public void numeroNegativoNaoDeveSerPositivo(){
        
        Paridade paridade = new Paridade();
        int numero = -6 ;
        
        assertFalse(paridade.ehNumeroPositivo(numero));
    }
    
}
