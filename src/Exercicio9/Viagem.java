
package Exercicio9;


public class Viagem {
    

    double consumo = 12 / 1 ; // 12 km / litro
    double tempo ; // hora
    double velocidade ; // km / h
    
    double distancia ; // tempo * velocidade 
    double totalLitros ; // distancia / 12 

    public Viagem(double tempoHoras, double velocidadeKmPorHora) {
        this.tempo = tempoHoras;
        this.velocidade =velocidadeKmPorHora;

    }

    
    public double calcularLitros (){
        
        this.distancia = this.velocidade * this.tempo ;
        this.totalLitros = this.distancia / 12 ;
  
        return this.totalLitros ;
    }
    
}
