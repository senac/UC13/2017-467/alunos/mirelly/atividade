
package Exercicio5;


public class Paridade {

    public Paridade() {
    }
    
   public boolean ehNumeroPar (int numero){
       
       return( numero %2 == 0);
   }
   
     public boolean ehNumeroImpar (int numero){
       
       return ( numero %2 == 1);
   }
     
     public boolean ehNumeroNegativo (int numero){
         
         return(numero < 0);
     }
     
      public boolean ehNumeroPositivo (int numero){
         
         return (numero >= 0);
     }
    
}
