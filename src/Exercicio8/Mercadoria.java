
package Exercicio8;


public class Mercadoria {

    
    private double preco ;
    private double quantidade ;

    public Mercadoria(double preco, double quantidade) {
        this.preco = preco;
        this.quantidade = quantidade;
    }

    
    
    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }


    
    
    
}
