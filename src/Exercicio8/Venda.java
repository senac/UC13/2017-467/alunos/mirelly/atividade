
package Exercicio8;

import java.util.ArrayList;
import java.util.List;


public class Venda {
    
    private List<Mercadoria> mercadorias;
    private Cliente cliente ;
    private double valorTotal;
    private String estado;

    public Venda() {
        this.mercadorias = new ArrayList<>();
    }

    public Venda(Cliente cliente, String estado) {
        this();
        this.cliente = cliente;
        this.estado = estado ;
    }
    
    public void fecharVenda (List<Mercadoria> lista){
        
        double total=0;
        
        for(Mercadoria m : lista){
            
            total += m.getPreco() * m.getQuantidade() ;
            
        }
        
        this.valorTotal = total ;
        
        
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<Mercadoria> getMercadorias() {
        return mercadorias;
    }

    public void setMercadorias(List<Mercadoria> mercadorias) {
        this.mercadorias = mercadorias;
    }
    
    
    
    
    
}
