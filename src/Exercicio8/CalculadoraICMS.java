
package Exercicio8;

import java.util.List;


public class CalculadoraICMS {
        
    public static double ALIQUOTA_RJ = 0.17 ;
    public static double ALIQUOTA_SP = 0.18 ;
    public static double ALIQUOTA_MA= 0.12 ;  

    public CalculadoraICMS() {
    }
    
    
    public double calcularICMS (Venda venda){
        
        String estado = venda.getEstado();
        double total = venda.getValorTotal();
        
 
      switch (estado) {  
       case "RJ" : return total  * ALIQUOTA_RJ;  
 
       case "MA" : return total  * ALIQUOTA_MA;  

       case "SP" : return total  * ALIQUOTA_SP;  

     }
      
 
        return 0;
    }
    
    
}
