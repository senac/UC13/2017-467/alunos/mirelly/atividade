
package Exercicio7;


public class Triangulo extends FormaGeometrica{
    
    double base ;
    double altura ;

    public Triangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }
    
    

    @Override
    public double getArea() {
      
        return (this.altura * this.base)/2 ;
    }
    
    
}
