
package Exercicio7;


public class Quadrado extends FormaGeometrica{
    
 double lado ;

    public Quadrado(double lado) {
        this.lado = lado;
    }
 
 

    @Override
    public double getArea() {
        
        
        return this.lado * this.lado ;
    }
 
 
 
 
}
