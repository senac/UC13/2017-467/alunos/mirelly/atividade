
package Exercicio7;

public class Retangulo extends FormaGeometrica{
    
    double ladoA ;
    double ladoB ;

    public Retangulo(double ladoA, double ladoB) {
        this.ladoA = ladoA;
        this.ladoB = ladoB;
    }


    @Override
    public double getArea() {

        return this.ladoA * this.ladoB ;
      
    }
    
}
