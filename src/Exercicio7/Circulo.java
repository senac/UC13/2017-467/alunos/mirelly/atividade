
package Exercicio7;


public class Circulo extends FormaGeometrica{
    
double raio;

    public Circulo(double raio) {
        this.raio = raio;
    }


    
    @Override
    public double getArea() {
        
        return 3.14 * (this.raio * this.raio) ;
    }
    
}
