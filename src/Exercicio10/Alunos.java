
package Exercicio10;


public class Alunos {
    
 
    private String nome ;
    private double nota1;
    private double nota2 ;
    private int situacao ;

    public Alunos() {
    }
    
    

    public Alunos(String nome, double nota1, double nota2) {
        this.nome = nome;
        this.nota1 = nota1;
        this.nota2 = nota2;
    }
    
      public double getMedia() {
         return (this.nota1 + this.nota2) / 2 ;
 
    }

    public int getSituacao() {
        
        if(getMedia() >= 7){
            this.situacao = CalculoAlunos.ALUNO_APROVADO ;
        }else{
            this.situacao = CalculoAlunos.ALUNO_REPROVADO ;
        }
 
        return situacao;
    }
    
      

    public double getNota2() {
        return nota2;
    }

    public void setNota2(double nota2) {
        this.nota2 = nota2;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getNota1() {
        return nota1;
    }

    public void setNota1(double nota1) {
        this.nota1 = nota1;
    }

  
    
    
    
    
    
}
