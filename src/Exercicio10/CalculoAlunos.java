
package Exercicio10;

import java.util.ArrayList;
import java.util.List;

public class CalculoAlunos {
    
    public static int ALUNO_APROVADO = 1 ;
    public static int ALUNO_REPROVADO = 0;

    public CalculoAlunos() {
        
    }
    
    public static void main(String[] args) {
        
        Alunos a = new Alunos("a", 5, 10);
        Alunos b = new Alunos("b", 8, 8);
        Alunos c = new Alunos("c", 5, 5);
        
        List<Alunos> lista = new ArrayList<>();
        lista.add(a);
        lista.add(b);
        lista.add(c);

        CalculoAlunos ca = new CalculoAlunos();
        
        int ax = ca.verificarMedia(lista);
        
    }  
    
    public int verificarMedia (List<Alunos> listaAlunos){
                
        int quantidadeAcimaMedia = 0;
        
        for (Alunos aluno : listaAlunos) {
            if(aluno.getSituacao() == ALUNO_APROVADO){
                
                   quantidadeAcimaMedia ++;              
            }
            
        }
  
        return quantidadeAcimaMedia;
    }

 
    
    
    
}
