
package Exercicio4;

import java.util.ArrayList;
import java.util.List;


public class Venda {
    
  
    private Vendedor vendedor ;
    private List<ItemVenda> itens ;

    public Venda() {
        this.itens = new ArrayList<>();
    }

    public Venda(Vendedor vendedor) {
        this();
        this.vendedor = vendedor;  

    }
   
    public double calcularComissao ( List<ItemVenda> itens ){

          double total = 0;
          
          for(ItemVenda item : itens){
              
              total += item.getQuantidade() * item.getValorUnit();
              
          }
         
          double comissao = total * 0.05 ;
          
          return comissao ;
      }
    
    public void adicionarItem(Peca peca , int quantidade , double valor){
        this.itens.add(new ItemVenda(peca, quantidade, valor));
    }
    
    public void removerItem(){
        
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public List<ItemVenda> getItens() {
        return itens;
    }

    public void setItens(List<ItemVenda> itens) {
        this.itens = itens;
    }
    
    public double getTotal(){
        double total = 0 ;
        
        for(ItemVenda itemVenda : itens){
            
            total += itemVenda.getTotal();
        }
        
        
        return total;
    }

 
    
    
    
    
    

}
