
package Exercicio4;

import java.util.ArrayList;
import java.util.List;


public class Peca {
    
    private int codigo;
    private String descricao ;
  

    public Peca() {
    }

    public Peca(int codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }
    
    

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    
}
