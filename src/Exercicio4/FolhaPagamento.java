
package Exercicio4;

import java.util.HashMap;
import java.util.List;

public class FolhaPagamento {
    
    private HashMap<Integer,Double> comissoes = new HashMap<>();
    
    public static double TAXA_COMISSAO = 0.05 ;

    public FolhaPagamento() {
    }
    
    public void calcularComissao (List<Venda>lista){
        
       for (Venda v : lista){
           int id = v.getVendedor().getId();
           
           if(!comissoes.containsKey(v.getVendedor().getId())){
               comissoes.put(id, v.getTotal()*TAXA_COMISSAO);
           }else{
               double comissaoParcial = comissoes.get(id).doubleValue();
               comissaoParcial += v.getTotal() * TAXA_COMISSAO;
               comissoes.put(id, comissaoParcial);
           }
           
       }
        
    }
    
    public double getComissaoVendedor (Vendedor vendedor){
        return this.comissoes.get(vendedor.getId());
    }
    
    
}
