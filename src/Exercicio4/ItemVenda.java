
package Exercicio4;

public class ItemVenda {

    private Peca peca;
    private int quantidade ;
    private double valorUnit;

    public ItemVenda() {
    }
    
    

    public ItemVenda( Peca peca, int quantidade, double valorUnit) {
        this.peca = peca;
        this.quantidade = quantidade;
        this.valorUnit = valorUnit;
    }

    public double getValorUnit() {
        return valorUnit;
    }

    public void setValorUnit(double valorUnit) {
        this.valorUnit = valorUnit;
    }

    public Peca getPeca() {
        return peca;
    }

    public void setPeca(Peca peca) {
        this.peca = peca;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
    
    public double getTotal(){
        return this.quantidade * this.valorUnit ;
    }
    
    
    
    
    
  
}
