package Exercicio1;

import java.util.List;

public class IdadePessoas {


    public IdadePessoas(){
 

    }


    public Pessoa verificarMaisVelho(List<Pessoa> lista) {
        Pessoa maisVelho = lista.get(0);
  

        for (Pessoa pessoa : lista) {

            if (pessoa.getIdade() > maisVelho.getIdade()) {
                maisVelho = pessoa;
            }

        }
        return maisVelho;
    }
    
     public Pessoa verificarMaisNovo(List<Pessoa> lista) {
        Pessoa maisNovo = lista.get(0);

        for (Pessoa pessoa : lista) {
            if (pessoa.getIdade() < maisNovo.getIdade()) {
                maisNovo = pessoa;
            }
        }

        return maisNovo;
    }

 
    

}
