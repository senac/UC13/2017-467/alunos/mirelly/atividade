
package Exercicio1;

import java.util.ArrayList;
import java.util.List;



public class Pessoa {
    
    static List<Pessoa> lista = new ArrayList<>();
    private String nome;
    private int idade ;

    public Pessoa() {
    }

    public Pessoa(String nome, int idade) {
        this.nome = nome;
        this.idade = idade;
    }
    
    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    
    
    
    
}
