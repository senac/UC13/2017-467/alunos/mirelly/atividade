
package Exercicio3;


public class ConversorMoedas {
    
  
    private final double COTACAO_DOLLAR_US = 4.08;
    private final double COTACAO_DOLLAR_AU = 2.96;
    private final double COTACAO_EURO_EU = 4.8;
    
    public ConversorMoedas() {
    }


    public DolarAmericano converterParaDolarAmericano(Real real, DolarAmericano dolarAmericano){
       
       dolarAmericano.setValor(real.getValor() * COTACAO_DOLLAR_US);
        
        return dolarAmericano;
        
    }
    
    public Euro converterParaEuro (Real real , Euro euro){
        
        euro.setValor(real.getValor() * COTACAO_EURO_EU);
        
        
        return euro ;
    }
    
    public DolarAustraliano converterParaDolarAustraliano (Real real, DolarAustraliano dolarAustraliano){
        
        dolarAustraliano.setValor(real.getValor() * COTACAO_DOLLAR_AU );
        
        return dolarAustraliano;
    }



    
}
