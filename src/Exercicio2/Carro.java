
package Exercicio2;


public class Carro {
    
    private double custoFabrica;
    private double custoConsumidor ;
    final double porcetagemDistribuidor = 0.28;
    final double impostosCustoDeFabrica = 0.45;

    public Carro( double custoFabrica) {
        this.setCustoFabrica(custoFabrica);
    }

    public double getCustoFabrica() {
        return custoFabrica;
    }

    public void setCustoFabrica(double custoFabrica) {
        this.custoFabrica = custoFabrica;
    }

    public double getCustoConsumidor() {
        
        this.custoConsumidor = ( this.custoFabrica * this.impostosCustoDeFabrica ) +
                (this.custoFabrica * this.porcetagemDistribuidor) +
                this.custoFabrica;
        
        
        return custoConsumidor;
    }

    public void setCustoConsumidor(double custoConsumidor) {
        this.custoConsumidor = custoConsumidor;
    }
    
    
    
    
    
    
    
}
