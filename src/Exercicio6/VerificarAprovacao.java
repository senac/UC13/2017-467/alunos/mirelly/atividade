
package Exercicio6;


public class VerificarAprovacao {
    
    public static String ALUNO_APROVADO = "Aprovado";
    public static String ALUNO_REPROVADO = "Reprovado";
    public static String ALUNO_RECUPERACAO = "Recuperação";

    public VerificarAprovacao() {
    }

   public boolean verificarAlunoAprovado(Aluno aluno){
       
       return aluno.getNota() >=7  ;
   }
   
   public boolean verificarAlunoReprovado(Aluno aluno){
       
       return aluno.getNota() <4 ;
   }
   
   public boolean verificarAlunoRecuperacao (Aluno aluno){
       
       return aluno.getNota() >=4 && aluno.getNota() <= 6 ;
   }
   
   public String resultadoAprovacao (Aluno aluno){
       String resultado= null;
       
       if(verificarAlunoAprovado(aluno)){
           resultado = this.ALUNO_APROVADO ;
       }else if (verificarAlunoReprovado(aluno)){
           resultado = this.ALUNO_REPROVADO;
       }else 
           resultado = this.ALUNO_RECUPERACAO ;
       
       
       return resultado;
   }

    
}
